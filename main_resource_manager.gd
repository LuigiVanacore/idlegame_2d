extends Node


@export var worker_pool : int = 0
# Called when the node enters the scene tree for the first time.
func _ready():
	Event_Bus.subscribe("update_worker_pool",self,update_worker_pool) # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func update_worker_pool(value : int):
	if value < 0:
		value = 0
	worker_pool += value
	Event_Bus.publish("worker_pool_updated", worker_pool)
	
