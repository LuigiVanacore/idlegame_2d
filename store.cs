using Godot;
using System;
using System.Runtime.InteropServices;

public class store : Panel
{
	private int store_count = 0;
	private int store_profit = 2;

	private int store_cost = 5;

	private bool manager_unlocked = false;

	private int manager_cost = 7;

	private bool timer_running = false;

 
	public Action<store, int> onUnlockManager;

	
	

	public void UpdateUI()
	{
		var label = GetNode<Label>("StoreCountLabel");
		label.Text = store_count.ToString();
	}

	public void _on_Button_pressed()
	{
		if (!timer_running && store_count > 0)
		{
			GD.Print("Started Timer");
			timer_running = true;
			GetNode<Timer>("StoreTimer").Start();
			GetNode<Timer>("ProgressTimer").Start();
		}
	}

	public void StorePurchased(store _store)
	{
		 
			store_count += 1;
			UpdateUI();
		
	}

	public void UnlockManager()
	{
	
			manager_unlocked = true;
			var checkbox = GetNode<CheckBox>("UnlockManagerCheckbox");
			checkbox.Pressed = true;
		
	}

	public void _on_BuyButton_pressed()
	{
		GD.Print("Buy Button Pressed");
		var mainObj = GetTree().Root;
		var mainRoot = mainObj.GetNode<main>("main");
		mainRoot.BuyStore(store_cost,this);
	}

	public void _on_StoreTimer_timeout()
	{
	 
		timer_running = false;
		GetNode<Timer>("StoreTimer").Stop();
		
		GD.Print("Store Time updated");
		GetNode<Timer>("ProgressTimer").Stop();
		GD.Print("Progress timer updated");
		GetNode<ProgressBar>("ProgressBar").Set("value",0);
		GD.Print("Progress bar updated");
		var mainObj = GetTree().Root;
		var mainRoot = mainObj.GetNode<main>("main");
		mainRoot.AddToMoney(store_profit * store_count);
		if (manager_unlocked)
			_on_Button_pressed();

	}

	public void _on_ProgressTimer_timeout()
	{
	 
		var storeTimer = GetNode<Timer>("StoreTimer");
		var current_progress = (storeTimer.WaitTime - storeTimer.TimeLeft) / storeTimer.WaitTime;
		GetNode<ProgressBar>("ProgressBar").Set("value", current_progress);
		
	}
	
	public void _on_UnlockManagerCheckbox_pressed(){
		 
		var checkbox = GetNode<CheckBox>("UnlockManagerCheckbox");
		if (!manager_unlocked)
		{
			
			checkbox.Pressed = false;

			
			var mainObj = GetTree().Root;
			var mainRoot = mainObj.GetNode<main>("main");
			mainRoot.UnlockManager(this,manager_cost);
			 
		}
		else
		{
			 
			checkbox.Pressed = true;

		}
	}
	
  
	public override void _Ready()
	{
		UpdateUI();
		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
