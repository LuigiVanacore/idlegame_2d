extends Resource

class_name producer_resource

@export  var name : String

@export var icon : Texture2D

@export var product : product_resource

@export var producer_info : String

@export var worker_required : int

@export var resource_required_for_construction : Dictionary

@export var resource_input : Dictionary

@export var resource_output : Dictionary

@export var production_time : int

@export var total_output_quantity : int
