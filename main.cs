using Godot;
using System;
using System.Diagnostics;

public class main : Node
{
    public int money = 5;

    public Action<main> onStorePurchased;

    public Action<main> onManagerUnlocked;
 
 

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        //enableEvents();
        UpdateUI();
        
    }

    public void UpdateUI()
    {
        GetNode<Label>("MoneyLabel").Text = money.ToString();
    }

    public void AddToMoney(int amt)
    {
        money += amt;
        UpdateUI();
    }

    public void BuyStore(int amt,store storeObj)
    {
        GD.Print("Buy Store Happened.");
        if (amt <= money)
        {
            money -= amt;
            UpdateUI();
            storeObj.StorePurchased(storeObj);
        }
    }

    public void UnlockManager(store storeobj, int amt)
    {
        GD.Print("Attempt to unlock manager.");
        if (amt <= money)
        {
            money -= amt;
            storeobj.UnlockManager();
        }
    }
   

 
}
