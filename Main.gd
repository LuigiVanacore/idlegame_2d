extends Node


export var money = 5

signal STORE_PURCHASED
signal MANAGER_UNLOCKED
signal SHOW_WARNING

func _ready():
	SetupSignalListeners()
	UpdateUI()

func AddToMoney(amt):
	
	money += amt
	UpdateUI()
	
func UpdateUI():
	$MoneyLabel.text = "$" + str(money)

func OnStoreMakeMoney(amt):
	AddToMoney(amt)

func OnUnlockManager(_self,amt):
	if amt <= money:
		money -= amt
		UpdateUI()
		emit_signal("MANAGER_UNLOCKED",_self)
	else:
		emit_signal("SHOW_WARNING", "You don't have enough money to unlock the manager for this store")
func OnBuyStore(_self,amt):
	
	if amt <= money:
		money -= amt
		UpdateUI()
		emit_signal("STORE_PURCHASED",_self)
		
	else:
		emit_signal("SHOW_WARNING", "You don't have enough money to buy the store. You need: $ %s " % amt)
	
func SetupSignalListeners():
	for store in self.get_children():
		store.connect("PRODUCTION_COMPLETE",self,"OnStoreMakeMoney")
		store.connect("ON_BUY_STORE",self,"OnBuyStore")
		store.connect("ON_UNLOCK_MANAGER",self,"OnUnlockManager")
