extends Node

class_name global_enum

enum global_event {
	new_producer_added,
	new_producer_created,
	create_new_producer,
	producer_removed,
	new_product_ready,
	new_resource_added,
	resource_added,
	resoruce_removed,
	not_enough_resource_for_construction,
	new_building_requested,
	new_building_created,
	storage_full,
	food_consumed,
	starving
}

