extends Label

class_name FarmerLabel 


var total_grain : int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	Event_Bus.subscribe("new_product_ready",self, update_text)# Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func update_text(message : Dictionary):
	var resource_name = message.get("resource_name")
	total_grain += message.get("total_output_quantity")
	set_text(resource_name + ": " + str(total_grain))
