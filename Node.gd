extends Node

var ui_producer : PackedScene = load("res://UI/ui_producer/ui_producer.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	var new_ui_producer = ui_producer.instantiate()
	add_child(new_ui_producer) # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
