extends PopupDialog


func _ready():
	get_owner().connect("SHOW_WARNING",self,"PopupDialog")
	
func PopupDialog(message):
	$MessageLabel.text = message
	self.popup()
	$Timer.start()

func _on_Timer_timeout():
	self.hide()
