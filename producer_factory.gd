class_name Producer_Factory extends Node


@export var producer_scene : PackedScene = load("res://producer/producer.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

	
func create_producer(info_prudcer : Array) -> Node:
	var new_producer = producer_scene.instantiate()
	new_producer.producer_type = info_prudcer[0]
	new_producer.product_type = info_prudcer[1]
	return new_producer
	
