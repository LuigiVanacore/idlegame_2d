class_name test_producer extends Node

var farm : producer_resource
var grain : product_resource
var iron_mine : producer_resource
var iron : product_resource

@onready var ui_producer_factory : Node = $ui_producer_factory

# Called when the node enters the scene tree for the first time.
func _ready():
	farm = load("res://resources/producer/farm.tres")
	grain = load("res://resources/producer_resource/grain.tres") # Replace with function body.	
	iron_mine = load("res://resources/producer/iron_mine.tres")
	iron = load("res://resources/producer_resource/iron.tres")
	Event_Bus.subscribe(global_enum.global_event.new_producer_created,self, add_producer)



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func add_producer(producer : Producer):
	var new_ui_producer = ui_producer_factory.create_ui_producer(producer, self)
	

func _on_button_pressed():
	Event_Bus.publish(global_enum.global_event.create_new_producer,[farm, grain])

