class_name Producer extends Node


@export var producer_type : producer_resource 
@export var product_type : product_resource

#total number of producer
@export var total_producer_count : int = 1


# Called when the node enters the scene tree for the first time.
func _ready():
	$ProductionTimer.wait_time = producer_type.production_time 
	$ProductionTimer.start()# Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func output_producer():
	if consume_input_resource():
		var total_producted = producer_type.total_output_quantity * total_producer_count
		Event_Bus.publish(global_enum.global_event.new_product_ready, [ product_type.name, total_producted ])
		Event_Bus.subscribe(global_enum.global_event.storage_full, self, stop_production)

func _on_production_timer_timeout():
	output_producer()
	
func get_producer_name() -> String:
	return producer_type.name
	
func get_product_name() -> String:
	return product_type.name
	
func get_producer_icon() -> Texture2D:
	return producer_type.icon
	
func stop_production(messageData : Array):
	if product_type.name == messageData[0]:
		if messageData[1]:
			$ProductionTimer.stop()
		else:
			$ProductionTimer.start()
	
func add_producer():
	if (check_resource_required_for_construction()):
		storage_manager.remove_resources(producer_type.resource_required_for_construction)
		total_producer_count += 1
	else:
		Event_Bus.publish(global_enum.global_event.not_enough_resource_for_construction,self)
	
func remove_producer():
	total_producer_count -= 1
	if (total_producer_count == 0):
		total_producer_count = 1
	
func get_total_producer_count() -> int:
	return total_producer_count
	
	
func get_current_progress() -> float:
	var time_left = $ProductionTimer.time_left
	return (( $ProductionTimer.wait_time - $ProductionTimer.time_left ) / $ProductionTimer.wait_time) * 100
	
func consume_input_resource()->bool:
	if producer_type.resource_input.is_empty():
		return true
	return storage_manager.consume_resources(producer_type.resource_input)
	
func check_resource_required_for_construction()->bool:
	return storage_manager.has_resources(producer_type.resource_required_for_construction)
	
	
	

