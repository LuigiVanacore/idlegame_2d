extends Node


var farm : producer_resource
var grain : product_resource

# Called when the node enters the scene tree for the first time.
func _ready():
	farm = load("res://resources/producer/farm.tres")
	grain = load("res://resources/producer_resource/grain.tres")



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
