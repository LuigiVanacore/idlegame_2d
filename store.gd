extends Panel
var store_count = 0
var store_cost = 5
var store_profit = 2
var manager_unlocked = false
var manager_cost = 7
var timer_running = false
signal PRODUCTION_COMPLETE
signal ON_BUY_STORE
signal ON_UNLOCK_MANAGER
func _ready():
	get_owner().connect("STORE_PURCHASED",self, "StorePurchased")
	get_owner().connect("MANAGER_UNLOCKED",self, "UnlockManager")
	UpdateUI()

func UpdateUI():
	
	$StoreCountLabel.text =  str(store_count)
	
func _on_Button_pressed():
	if !timer_running and store_count > 0:
		timer_running = true
		get_node("StoreTimer").start()
		$ProgressTimer.start()

func StorePurchased(_store):
	if self == _store:
		store_count += 1
		UpdateUI()
func UnlockManager(_store):
	if self == _store:
		manager_unlocked = true
		$UnlockManagerCheckbox.pressed = true

func _on_BuyButton_pressed():
	
	# somebody wants to buy me... this is me and this is what I cost
	emit_signal("ON_BUY_STORE",self,store_cost)
	


func _on_StoreTimer_timeout():
	timer_running = false
	$StoreTimer.stop()
	$ProgressTimer.stop()
	$ProgressBar.set("value",0)
	emit_signal("PRODUCTION_COMPLETE", store_count * store_profit)
	UpdateUI()
	if manager_unlocked:
		_on_Button_pressed()
	

func _on_ProgressTimer_timeout():
	
	var current_progress = ( $StoreTimer.wait_time - $StoreTimer.time_left ) / $StoreTimer.wait_time
	
	$ProgressBar.set("value", current_progress)


func _on_UnlockManagerCheckbox_pressed():
	
	if !manager_unlocked:
		$UnlockManagerCheckbox.pressed = false
		emit_signal("ON_UNLOCK_MANAGER",self, manager_cost)
	else:
		$UnlockManagerCheckbox.pressed = true
