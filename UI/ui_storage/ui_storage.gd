class_name UI_Storage extends Control


var ui_resource_stored : PackedScene = load("res://UI/ui_resource_stored/ui_resource_stored.tscn")
@onready var ui_resource_stored_container : VBoxContainer = $Control/ScrollContainer/UI_Resource_Stored_Container

var ui_resource_dictionary : Dictionary

# Called when the node enters the scene tree for the first time.
func _ready():
	Event_Bus.subscribe(global_enum.global_event.new_resource_added,self,add_ui_resource_stored) # Replace with function body.
	Event_Bus.subscribe(global_enum.global_event.resource_added, self, update_ui_resource_stored)
	Event_Bus.subscribe(global_enum.global_event.resoruce_removed,self,update_ui_resource_stored)
	Event_Bus.subscribe(global_enum.global_event.storage_full,self,update_isfull_ui_resource_stored)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func add_ui_resource_stored(resourceData : Array):
	var new_ui_resource_stored = ui_resource_stored.instantiate()
	ui_resource_stored_container.add_child(new_ui_resource_stored)
	new_ui_resource_stored.update_text(resourceData)
	ui_resource_dictionary[resourceData[0]] = new_ui_resource_stored
	
func update_ui_resource_stored(resourceData : Array):
	ui_resource_dictionary[resourceData[0]].update_text(resourceData)

func update_isfull_ui_resource_stored(messageData : Array):
	ui_resource_dictionary[messageData[0]].set_is_full(messageData[1])
