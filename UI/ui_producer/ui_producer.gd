class_name UI_Producer extends Control


@onready var producer : Producer
var producer_name : String
var product_name : String

@onready var label_producer_name : Label = $Control2/Label_Producer_Name
@onready var label_total_producer : Label = $Control/Label_Total_Producer
@onready var label_total_output : Label = $Control4/Label_Total_Output
@onready var progress_bar : ProgressBar = $Control3/ProgressBar
@onready var producer_icon : TextureRect = $Control2/TextureRect_Producer_Icon

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (producer != null):
		update_total_producer_label()
		update_progress_bar()


func add_producer():
	pass


func bind_producer(_producer : Producer):
	producer = _producer
	producer_name = producer.get_producer_name()
	product_name = producer.get_product_name()
	label_producer_name.text = producer_name
	producer_icon.texture = producer.get_producer_icon()


	
func update_total_producer_label():
	label_total_producer.text = "Total: " + str(producer.get_total_producer_count())
	
	
	
func update_progress_bar():
	progress_bar.value = producer.get_current_progress()

func _on_button_plus_pressed():
	Event_Bus.publish(global_enum.global_event.new_producer_added,producer) # Replace with function body.
	
func _on_button_minus_pressed():
		Event_Bus.publish(global_enum.global_event.producer_removed,producer) # Replace with function body. # Replace with function body.
