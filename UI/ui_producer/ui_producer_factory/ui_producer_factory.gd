class_name UI_Producer_Factory extends Node


var ui_producer_scene : PackedScene = preload("res://UI/ui_producer/ui_producer.tscn")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func create_ui_producer(producer : Producer, node_master : Node) -> Node:
	var new_ui_producer = ui_producer_scene.instantiate()
	node_master.add_child(new_ui_producer)
	new_ui_producer.bind_producer(producer)
	return new_ui_producer

