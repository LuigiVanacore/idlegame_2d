extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	Event_Bus.subscribe("worker_pool_updated",self,update_worker_label) # Replace with function body.
	update_worker_label()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	

func update_worker_label():
	$WorkerLabel.text = "Worker: " + str(WorkerManager.get_total_workers())
