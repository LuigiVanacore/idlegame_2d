class_name UI_Producer_List extends Control


@onready var container_producer : VBoxContainer = $Control_Producer_List/ScrollContainer/VBoxContainer

@export var producer_ui_scene : PackedScene

var ui_producer_factory : UI_Producer_Factory

# Called when the node enters the scene tree for the first time.
func _ready():
	ui_producer_factory = UI_Producer_Factory.new()
	Event_Bus.subscribe(global_enum.global_event.new_producer_created,self, add_ui_producer) # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func add_ui_producer(producer : Producer):
	ui_producer_factory.create_ui_producer(producer, container_producer)
	
