extends Control


@onready var label_product_name : Label = $Control/Label_Product_Name
@onready var label_product_quantity : Label = $Control2/Label_Product_Quantity

@onready var red_font_setting : LabelSettings = load("res://assets/label_settings/red_font_setting.tres")
@onready var white_font_setting : LabelSettings = load("res://assets/label_settings/white_font_setting.tres")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func update_text(resourceData : Array):
	label_product_name.text = resourceData[0]
	label_product_quantity.text = str(resourceData[1])

func set_is_full(is_full : bool):
	if (is_full):
		label_product_name.label_settings = red_font_setting
		label_product_quantity.label_settings = red_font_setting
	else:
		label_product_name.add_theme_color_override("font_color",Color.BLACK)
		label_product_quantity.add_theme_color_override("font_color",Color.BLACK)
