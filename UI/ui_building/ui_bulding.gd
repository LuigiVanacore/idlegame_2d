class_name UI_Building extends Control


@export var producer_resource : producer_resource
@export var product_resource : product_resource

@onready var label_building_name : Label = $Label_Building_Name
@onready var label_building_info : Label = $Label_Building_Info
@onready var label_building_Requirement : Label = $Label_Building_Requirement

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass



func _on_button_build_pressed():
	Event_Bus.publish(global_enum.global_event.new_building_requested) # Replace with function body.

func ui_building_constructor(_producer_resource : producer_resource, _product_resource : product_resource):
	producer_resource = _producer_resource
	product_resource = _product_resource
	label_building_name.text = producer_resource.name
	label_building_info.text = producer_resource.info
	
