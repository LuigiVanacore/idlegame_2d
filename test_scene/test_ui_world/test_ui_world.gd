extends Node



var farm : producer_resource
var grain : product_resource
var iron_mine : producer_resource
var iron : product_resource
var logging_camp : producer_resource
var wood : product_resource

# Called when the node enters the scene tree for the first time.
func _ready():
	farm = load("res://resources/producer/farm.tres")
	grain = load("res://resources/product_resource/food.tres") # Replace with function body.	
	iron_mine = load("res://resources/producer/iron_mine.tres")
	iron = load("res://resources/product_resource/iron.tres")
	logging_camp = load("res://resources/producer/logging_camp.tres")
	wood = load("res://resources/product_resource/wood.tres")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	Event_Bus.publish(global_enum.global_event.create_new_producer,[farm, grain])
	Event_Bus.publish(global_enum.global_event.create_new_producer,[iron_mine, iron])
	Event_Bus.publish(global_enum.global_event.create_new_producer,[logging_camp, wood]) # Replace with function body.
