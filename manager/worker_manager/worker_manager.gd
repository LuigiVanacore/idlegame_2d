class_name Worker_Manager extends Node


@export var total_workers : int = 100
@export var workers_employed : int = 0

@export var consume_food_wait_time : int = 10

@export var hungry_quantity_per_worker : int = 2

@onready var timer_consume_food : Timer

# Called when the node enters the scene tree for the first time.
func _ready():
	timer_consume_food = Timer.new()
	add_child(timer_consume_food)
	$Timer_Consume_Food.wait_time = consume_food_wait_time
	$Timer_Consume_Food.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func consume_food():
	if (storage_manager.remove_resource("Food", total_workers * hungry_quantity_per_worker)):
		Event_Bus.publish(global_enum.global_event.food_consumed)
		print_debug("eating...")
	else:
		Event_Bus.publish(global_enum.global_event.starving)
		starving()
		
	

func has_workers_free() -> bool:
	if ( ( total_workers - workers_employed) > 0 ):
		return true
	else:
		return false 
		
		
func employe_workers(total_workers_to_employee : int ) -> bool:
	if (has_workers_free() and (( workers_employed + total_workers_to_employee) < total_workers)):
		workers_employed += total_workers_to_employee
		return true
	else:
		return false
		
func get_total_workers()->int:
	return total_workers
	
func starving():
	print_debug("starving...")


func _on_timer_consume_food_timeout():
	consume_food() 
