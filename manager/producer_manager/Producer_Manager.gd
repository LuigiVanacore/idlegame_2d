class_name Producer_Manager extends Node


@export var producer_scene : PackedScene

var producer_factory : Producer_Factory


# Called when the node enters the scene tree for the first time.
func _ready():
	producer_scene = load("res://producer/producer.tscn")
	producer_factory = Producer_Factory.new()
	Event_Bus.subscribe(global_enum.global_event.new_producer_added,self,add_producer)  # Replace with function body.
	Event_Bus.subscribe(global_enum.global_event.create_new_producer, self, create_producer)
	Event_Bus.subscribe(global_enum.global_event.producer_removed, self, remove_producer)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func create_producer(info_prudcer : Array):
	var new_producer = producer_factory.create_producer(info_prudcer)
	self.add_child(new_producer)
	Event_Bus.publish(global_enum.global_event.new_producer_created, new_producer)
		
func add_producer(producer : Producer):
	producer.add_producer()
	
func remove_producer(producer : Producer):
	producer.remove_producer()
	


	
