class_name Storage_Manager extends Node


@export var max_total_product_quantity : int = 1000
var storage : Dictionary
# Called when the node enters the scene tree for the first time.
func _ready():
	Event_Bus.subscribe(global_enum.global_event.new_product_ready, self, add_resource) # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func add_resource(resourceData : Array):
	var resource_name = resourceData[0]
	var resource_quantity = resourceData[1]
	
	if ( storage.has(resource_name)):
		var total_resource_quantity = storage[resource_name]
		total_resource_quantity += resource_quantity
		total_resource_quantity = check_max_total_quantity(total_resource_quantity, resource_name)
		storage[resource_name] = total_resource_quantity
		resourceData[1] = total_resource_quantity
		Event_Bus.publish(global_enum.global_event.resource_added, resourceData)
	else:
		resource_quantity = check_max_total_quantity(resource_quantity, resource_name)
		storage[resource_name] = resource_quantity
		resourceData[1] = resource_quantity
		Event_Bus.publish(global_enum.global_event.new_resource_added, resourceData)


func check_max_total_quantity(resource_quantity : int, resource_name : String)->int:
	if ( resource_quantity >= max_total_product_quantity):
		resource_quantity = max_total_product_quantity
		Event_Bus.publish(global_enum.global_event.storage_full,[resource_name,true])
	return resource_quantity

func remove_resource(resource_name : String, resource_quantity : int)->bool:
	if ( storage.has(resource_name)):
		var total_resource_quantity = storage[resource_name]
		if ((total_resource_quantity-resource_quantity)<0):
			return false
		if (total_resource_quantity == max_total_product_quantity):
			Event_Bus.publish(global_enum.global_event.storage_full,[resource_name,false])
		total_resource_quantity -= resource_quantity
		storage[resource_name] = total_resource_quantity
		Event_Bus.publish(global_enum.global_event.resoruce_removed, [resource_name, total_resource_quantity])
		return true
	return false
	
func remove_resources(resources : Dictionary)->bool:
	var keys = resources.keys()
	for key in keys:
		if (!remove_resource(key, resources.get(key))):
			return false
	return true
	
func consume_resources(resources : Dictionary)->bool:
	if !storage.has_all(resources.keys()):
		return false
	
	return false
	
func has_resources(resources : Dictionary)->bool:
	var keys = resources.keys()
	for key in keys:
		if (storage.has(key)):
			var quantity = storage.get(key)
			if (quantity < resources.get(key)):
				return false
		else:
			return false
	return true

