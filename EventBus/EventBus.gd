extends Node

## EventBus
##
## Class that manage event signal
## It is an autoload node

var receivers : Dictionary = {}
var events : Array = []

class Event:
	var receiver = null
	var method = null
	var value = null

func _ready():
	pass

func _process(delta):
	_process_events();


func _process_events():
	if events.size() == 0:
		return

	var event = events.pop_front()
	event.method.call(event.value)



func publish(event_name, value = null):
	if !receivers.has(event_name) or receivers[event_name] == null:
		return
	var event_receivers = receivers[event_name]
	for receiver in event_receivers.keys():
		var event = Event.new()
		event.receiver = receiver
		event.method = event_receivers[receiver]
		event.value = value
		events.push_back(event)


## subscribe
## parameters:
## event_name: name of the event
## receiver: node receiver
## method: method that will be called
func subscribe(event_name, receiver, method):
	if !receivers.has(event_name):
		receivers[event_name] = {}
	receivers[event_name][receiver] = method

func unsubscribe(event_name, receiver):
	receivers[event_name].erase(receiver)

func queue_size():
	return events.size()
